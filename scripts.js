window.addEventListener("DOMContentLoaded", () => {
    let checkbox = document.getElementById('price-check');
    checkbox.addEventListener('change' , (event) => {
        const checked = event.target.checked;
        if (checked) {
            document.getElementById('basic').innerText = 19.99;
            document.getElementById('professional').innerText = 24.99;
            document.getElementById('master').innerText = 39.99;
        }else {
            document.getElementById('basic').innerText = 199.99;
            document.getElementById('professional').innerText = 249.99;
            document.getElementById('master').innerText = 399.99;
        }
    })
});
